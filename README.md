<p align="center">
  <img alt="Portfolio eduardo silva" src=".github/banner.png" width="100%">
</p>

<p align="center">
  <a href="#deploy-">Deploy</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#tecnologias-">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#projeto-">Projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#memo-licença">Licença</a>
</p>

# Deploy 🚀

 [Portfolio Web](https://portfolioweb-eduardo.vercel.app/)

# Tecnologias 🧰

Tecnologias usadas:

- html
- Javascript
- Css utilizando Sass

# Projeto 💻

O projeto é uma aplicação que apresenta projetos web

## :memo: Licença

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.

---
