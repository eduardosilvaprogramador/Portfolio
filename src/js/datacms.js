
const $listProjects = document.querySelector('.projects__grid')

const  showProjects = projects => {
    let templet = "";

    projects.slice(0, 6).map(
        (project) =>
            (templet += `
                <div class="project">
                    <img src="${project.imageurl}" alt="">
                    <h3>${project.title}</h3>
                    <p class="projects__description">${project.description}</p>
                    <ul class="projects__stack">${project.skill}</ul>
                    <a href="${project.repository}" class="link link--icon">
                        <i class="fa-solid fa-code"></i> 
                    </a>
                </div>
        `)
    );
    $listProjects.insertAdjacentHTML( "beforeend", templet);
}


(async () => {
    const u = 'ba2b603e36eeb3ba2a443d206e9a9d'
    await fetch("https://graphql.datocms.com/", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${u}`,
        },
        body: JSON.stringify({
            query: ` {
              allProjects(filter: {public: {eq: true}}) {
                    title
                    subtitle
                    imageurl {
                        url
                    }
                    description
                    repository
                    skill
                    demo
                }
            }`,
        }),
    })
        .then((res) => res.json())
        .then((res) => showProjects(res.data.allProjects));
})()
