const $div_main = document.querySelector('#theme')
const $toggle_theme = document.querySelector('.nav__theme')

const setTheme = (themeName) => {
    localStorage.setItem('theme' , themeName)
    $div_main.className = themeName
}

const toggleTheme = () => {
    if (localStorage.getItem('theme') === 'dark'){
        setTheme('light')
        $toggle_theme.innerHTML = '<i class="fa-solid fa-moon"></i>'
    } else {
        setTheme('dark')
        $toggle_theme.innerHTML = '<i class="fa-solid fa-sun"></i>'
    }
}

(() => {
    if(localStorage.getItem('theme') === 'dark') {
        setTheme('dark')
    } else {
        setTheme('light')
    }
})()
